let myVariable = 1;

// ne pas utiliser !
console.log(1 == true); // true
// utilise plutôt celui ci
console.log(1 === true); // false

console.log(1 !== 2); // true

console.log(1 < 2); // true
console.log(1 <= 2); // true

console.log(true && false); // false
console.log(true && true); // true

console.log( myVariable >= 1 && myVariable <= 10 ); // true si var est comprise entre 1 et 10

console.log(true || false); // true
console.log(true || true); // true
console.log(false || false); // false

console.log( myVariable === 1 || myVariable === 5); // true si var est 1 ou 5;

console.log( (myVariable >= -1 && myVariable <= 1) || myVariable === 10 ); // true si var est comprise en -1 et 1 ou est égal a 10

if( 1===1 ) {
  console.log("kiki");
}