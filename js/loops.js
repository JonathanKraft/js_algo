for(let x = 0; x < 5; x++) {
  console.log(x);
}

// Faire une boucle pour déclarer votre amour à un animal (qui peut être humain).
// Donc une boucle qui vas répeter "vraiment" autant de fois que vous l'aimez,
// (Pour avoir pas exemple a la fin : Mon toutout, je t'aime vraiment, vraiment, vraiment beaucoup)

let loveLetter = "Monsieurs le chat, je t'aime";
for(let x = 0; x < 6; x++) {
  loveLetter += " vraiment";
}
loveLetter += " beaucoup.";
console.log(loveLetter);

let go = true;
while(go) {
  console.log("in my while");
  go = false;
}

// Refaire la première boucle en for, mais en while.

let lettreDamour = "Le chien, je t'aime";
let y = 0;
while(y < 6) {
  lettreDamour += " vraiment,";
  y++;
}
lettreDamour += " beaucoup.";
console.log(lettreDamour);